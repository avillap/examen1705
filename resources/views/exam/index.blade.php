@extends('layouts.app')

@section('content')
<div class="container">

<h1>Lista de Examenes</h1>

    <table class="table table-bordered">
    <tr>
        <th>Examen</th>
        <th>Fecha</th>
        <th>Materia</th>
        <th>Usuario</th>
        <th>Opciones</th>
    </tr>
    @foreach ($exams as $exam)
    <tr>
        <td>{{ $exam->title }}</td>
        <td>{{ $exam->date }}</td>
        <td>{{ $exam->user->name }}</td>
        <td>{{ $exam->module->name }}</td>
        <td>
            <a href="/exams/{{ $exam->id }}/remember " class="btn btn-info" type="submit">Recordar</a>
            <a href="/exams/{{ $exam->id}}/destroy " class="btn-danger" type="submit">Borrar</a>
        </td>
    </tr>
    @endforeach
</table>

{{ $exams->links() }}
</div>
@endsection