<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Exam;

class ExamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $exams = Exam::paginate(7);
        return view('exam/index', ['exams' => $exams]);
    }
}
