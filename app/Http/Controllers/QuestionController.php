<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Question;
use \App\Module;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $questions = Question::paginate(15);
        return view('question/index', ['questions' => $questions]);
    }
    public function create()
    {
        $modules = Module::all();
        return view('question.create',  ['modules' => $modules]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'text' => 'required|max:25','a' => 'required|max:25',
            'b' => 'required|max:25','c' => 'required|max:25',
            'd' => 'required|max:25','answer' => 'required|max:1',
        ]);
        $question = new Question();
        $question->fill($request->all());
        $question->save();
        return redirect('/questions');
    }

}