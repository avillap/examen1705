<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = [
        'title', 'date'
    ];
    public function questions()
    {
        return $this->belongsToMany('App\Question', 'exam_question')
          ->withTimestamps();
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function module()
    {
        return $this->belongsTo('App\Module', 'module_id');
    }
}